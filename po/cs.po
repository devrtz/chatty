# Jaroslav Svoboda <multi.flexi@seznam.cz>, 2018. #zanata
# Jaroslav Svoboda <multi.flexi@seznam.cz>, 2019. #zanata
# Jaroslav Svoboda <multi.flexi@seznam.cz>, 2020. #zanata
msgid ""
msgstr ""
"Project-Id-Version: purism-chatty\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-03-05 12:35+0100\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2020-01-14 12:50-0500\n"
"Last-Translator: Jaroslav Svoboda <multi.flexi@seznam.cz>\n"
"Language-Team: Czech\n"
"Language: cs\n"
"X-Generator: Zanata 4.6.2\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2\n"

#: data/sm.puri.Chatty.appdata.xml.in:6 data/sm.puri.Chatty.desktop.in:3
#: src/chatty-window.c:483 src/ui/chatty-window.ui:210
msgid "Chats"
msgstr "Konverzace"

#: data/sm.puri.Chatty.appdata.xml.in:7
msgid "A messaging application"
msgstr "Komunikační aplikace"

#: data/sm.puri.Chatty.appdata.xml.in:9
msgid "Chats is a messaging application supporting XMPP and SMS."
msgstr "Chats je aplikace na posílání XMPP a SMS zpráv"

#: data/sm.puri.Chatty.appdata.xml.in:16
msgid "Chats message window"
msgstr "Okno zprávy Chats"

#: data/sm.puri.Chatty.appdata.xml.in:29
msgid "Initial development release of chatty with support for xmpp and sms."
msgstr "Prvotní vývojářská verze Chatty bude podoporovat XMPP a SMS."

#: data/sm.puri.Chatty.desktop.in:5
msgid "sm.puri.Chatty"
msgstr "sm.puri.Chatty"

#: data/sm.puri.Chatty.desktop.in:6
msgid "SMS and XMPP chat application"
msgstr "SMS a XMPP konverzační aplikace"

#: data/sm.puri.Chatty.desktop.in:7
msgid "XMPP;SMS;chat;jabber;messaging;modem"
msgstr "XMPP;SMS;chat;jabber;messaging;modem"

#: data/sm.puri.Chatty.gschema.xml:7 data/sm.puri.Chatty.gschema.xml:8
msgid "Whether the application is launching the first time"
msgstr "Zdali je aplikace spouštěna poprvé"

#: data/sm.puri.Chatty.gschema.xml:13
msgid "Send message read receipts"
msgstr "Odesílat potvrzení o přečtení zprávy"

#: data/sm.puri.Chatty.gschema.xml:14
msgid "Whether to send the status of the message if read"
msgstr "Zdali odesílat stav zprávy po přečtení"

#: data/sm.puri.Chatty.gschema.xml:19
msgid "Message carbon copies"
msgstr "Kopie zpráv"

#: data/sm.puri.Chatty.gschema.xml:20 src/ui/chatty-settings-dialog.ui:151
msgid "Share chat history among devices"
msgstr "Sdílet historii konverzací se všemi zařízeními"

#: data/sm.puri.Chatty.gschema.xml:25
msgid "Enable Message Archive Management"
msgstr "Zapnout správu archivu zpráv"

#: data/sm.puri.Chatty.gschema.xml:26
msgid "Enable MAM archive synchronization from the server"
msgstr "Zapnout MAM synchronizaci archivu ze serveru"

#: data/sm.puri.Chatty.gschema.xml:31
msgid "Send typing notifications"
msgstr "Odesílat upozornění o psaní"

#: data/sm.puri.Chatty.gschema.xml:32
msgid "Whether to Send typing notifications"
msgstr "Zdali odesílat upozornění o psaní"

#: data/sm.puri.Chatty.gschema.xml:37 data/sm.puri.Chatty.gschema.xml:38
msgid "Mark offline users differently"
msgstr "Označit offline uživatele jinak"

#: data/sm.puri.Chatty.gschema.xml:43 data/sm.puri.Chatty.gschema.xml:44
msgid "Mark Idle users differently"
msgstr "Označit nečinné uživatele jinak"

#: data/sm.puri.Chatty.gschema.xml:49 data/sm.puri.Chatty.gschema.xml:50
msgid "Indicate unknown contacts"
msgstr "Označovat neznámé kontakty"

#: data/sm.puri.Chatty.gschema.xml:55
msgid "Convert text to emoticons"
msgstr "Převést text na emotikony"

#: data/sm.puri.Chatty.gschema.xml:56
msgid "Convert text matching emoticons as real emoticons"
msgstr "Převést odpovídající emotikony v textu na opravdové emotikony"

#: data/sm.puri.Chatty.gschema.xml:61
msgid "Enter key sends the message"
msgstr "Klávesa Enter odešle zprávu"

#: data/sm.puri.Chatty.gschema.xml:62
msgid "Whether pressing Enter key sends the message"
msgstr "Zdali zmáčknutí klávesy Enter odešle zprávu"

#: src/chatty-application.c:61
msgid "Show release version"
msgstr "Zobrazit verzi"

#: src/chatty-application.c:62
msgid "Start in daemon mode"
msgstr "Spustit v módu démona"

#: src/chatty-application.c:63
msgid "Disable all accounts"
msgstr "Deaktivovat všechny účty"

#: src/chatty-application.c:64
msgid "Enable libpurple debug messages"
msgstr "Zapnout debugovací zprávy libpurple"

#: src/chatty-application.c:65
msgid "Enable verbose libpurple debug messages"
msgstr "Zapnout detailnější debugovací zprávy libpurple"

#: src/chatty-buddy-list.c:804
msgid "Disconnect group chat"
msgstr "Odpojit skupinovou konverzaci"

#: src/chatty-buddy-list.c:805
msgid "This removes chat from chats list"
msgstr "Tímto odstraníte koverzaci ze seznamu"

#: src/chatty-buddy-list.c:809
msgid "Delete chat with"
msgstr "Smazat chat s"

#: src/chatty-buddy-list.c:810
msgid "This deletes the conversation history"
msgstr "Toto vymaže historii konverzace"

#: src/chatty-buddy-list.c:821 src/chatty-purple-request.c:190
#: src/dialogs/chatty-settings-dialog.c:448
#: src/dialogs/chatty-user-info-dialog.c:76 src/ui/chatty-dialog-join-muc.ui:16
#: src/ui/chatty-dialog-muc-info.ui:56
msgid "Cancel"
msgstr "Zrušit"

#: src/chatty-buddy-list.c:823
msgid "Delete"
msgstr "Smazat"

#: src/chatty-buddy-list.c:1102
msgid "Me: "
msgstr "Já:"

#: src/chatty-connection.c:76
msgid "Login failed"
msgstr "Přihlášení selhalo"

#: src/chatty-connection.c:82
msgid "Please check ID and password"
msgstr "Prosim, zkontolujte své ID a heslo"

#: src/chatty-conversation.c:1458
msgid "Owner"
msgstr "Vlastník"

#: src/chatty-conversation.c:1461
msgid "Moderator"
msgstr "Moderátor"

#: src/chatty-conversation.c:1464
msgid "Member"
msgstr "Člen"

#: src/chatty-conversation.c:2161
#, c-format
msgid "New message from %s"
msgstr "Nová zpráva od %s"

#: src/chatty-message-list.c:73 src/chatty-message-list.c:78
msgid "This is a IM conversation."
msgstr "Toto je IM konverzace."

#: src/chatty-message-list.c:74 src/chatty-message-list.c:84
msgid "Your messages are not encrypted,"
msgstr "Vaše zprávy nejsou šifrovány,"

#: src/chatty-message-list.c:75
msgid "ask your counterpart to use E2EE."
msgstr "požádejte váš protějšek o užití E2EE."

#: src/chatty-message-list.c:79
msgid "Your messages are secured"
msgstr "Vaše zprávy jsou zabezpečeny"

#: src/chatty-message-list.c:80
msgid "by end-to-end encryption."
msgstr "E2E šifrováním."

#: src/chatty-message-list.c:83
msgid "This is a SMS conversation."
msgstr "Toto je SMS konverzace."

#: src/chatty-message-list.c:85
msgid "and carrier rates may apply."
msgstr "a může být zpoplatněno operátorem."

#: src/chatty-notify.c:80
msgid "Open Message"
msgstr "Otevřít zprávu"

#: src/chatty-notify.c:85
msgid "Message Received"
msgstr "Zpráva obdržena"

#: src/chatty-notify.c:91
msgid "Message Error"
msgstr "Chyba zprávy"

#: src/chatty-notify.c:97
msgid "Account Info"
msgstr "Informace o účtu"

#: src/chatty-notify.c:103
msgid "Account Connected"
msgstr "Účet připojen"

#: src/chatty-notify.c:110
msgid "Open Account Settings"
msgstr "Otevřít nastavení účtu"

#: src/chatty-notify.c:113
msgid "Account Disconnected"
msgstr "Účet odpojen"

#: src/chatty-purple-notify.c:41
msgid "Close"
msgstr "Zavřít"

#: src/chatty-purple-request.c:185
msgid "Save File..."
msgstr "Uložit soubor..."

#: src/chatty-purple-request.c:186
msgid "Open File..."
msgstr "Otevřít soubor..."

#: src/chatty-purple-request.c:192
msgid "Save"
msgstr "Uložit"

#: src/chatty-purple-request.c:192 src/dialogs/chatty-settings-dialog.c:447
#: src/dialogs/chatty-user-info-dialog.c:75
msgid "Open"
msgstr "Otevřít"

#: src/chatty-utils.c:48
msgid "About "
msgstr "O aplikaci"

#: src/chatty-utils.c:49
msgid "Less than "
msgstr "Méně než"

#: src/chatty-utils.c:50
msgid " seconds"
msgstr "sekundy"

#: src/chatty-utils.c:51
msgid " minute"
msgstr "minuta"

#: src/chatty-utils.c:52
msgid " minutes"
msgstr "minuty"

#: src/chatty-utils.c:53
msgid " hour"
msgstr "hodina"

#: src/chatty-utils.c:54
msgid " hours"
msgstr "hodiny"

#: src/chatty-utils.c:55
msgid " day"
msgstr "den"

#: src/chatty-utils.c:56
msgid " days"
msgstr "dny"

#: src/chatty-utils.c:57
msgid " month"
msgstr "měsíc"

#: src/chatty-utils.c:58
msgid " months"
msgstr "měsíce"

#: src/chatty-utils.c:59
msgid " year"
msgstr "rok"

#: src/chatty-utils.c:60
msgid " years"
msgstr "léta"

#: src/chatty-utils.c:64
msgid "s"
msgstr "s"

#: src/chatty-utils.c:65 src/chatty-utils.c:66
msgid "m"
msgstr "m"

#: src/chatty-utils.c:67 src/chatty-utils.c:68
msgid "h"
msgstr "h"

#: src/chatty-utils.c:69 src/chatty-utils.c:70
msgid "d"
msgstr "d"

#: src/chatty-utils.c:71
msgid "mo"
msgstr "měs"

#: src/chatty-utils.c:72
msgid "mos"
msgstr "měs"

#: src/chatty-utils.c:73 src/chatty-utils.c:74
msgid "y"
msgstr "r"

#: src/chatty-utils.c:177
msgid "Over"
msgstr "Více než"

#: src/chatty-utils.c:181
msgid "Almost"
msgstr "Téměř"

#: src/chatty-window.c:102 src/chatty-window.c:107 src/chatty-window.c:112
msgid "Choose a contact"
msgstr "Vybrat kontakt"

#: src/chatty-window.c:103
msgid ""
"Select an <b>SMS</b> or <b>Instant Message</b> contact with the <b>\"+\"</b> "
"button in the titlebar."
msgstr ""
"Vyberte kontakt pro <b>SMS</b> nebo <b>Instant Message</b> tlačítkem "
"<b>\"+\"</b> v záhlaví."

#: src/chatty-window.c:108
msgid ""
"Select an <b>Instant Message</b> contact with the \"+\" button in the "
"titlebar."
msgstr "Vyberte kontakt pro <b>Instant Message</b> tlačítkem \"+\"  v záhlaví."

#: src/chatty-window.c:113
msgid "Start a <b>SMS</b> chat with with the \"+\" button in the titlebar."
msgstr "Vyberte kontakt pro <b>SMS</b> tlačítkem \"+\"  v záhlaví."

#: src/chatty-window.c:114 src/chatty-window.c:118
msgid ""
"For <b>Instant Messaging</b> add or activate an account in "
"<i>\"preferences\"</i>."
msgstr ""
"Pro <b>Instant Messaging</b> přidejte nebo aktivujte účet v "
"<i>\"nastavení\"</i>."

#: src/chatty-window.c:117
msgid "Start chatting"
msgstr "Začít konverzovat"

#: src/chatty-window.c:485
msgid "An SMS and XMPP messaging client"
msgstr "SMS a XMPP konverzační klient"

#: src/chatty-window.c:492
msgid "translator-credits"
msgstr "překladatel"

#: src/chatty-window.c:627
#, c-format
msgid "Authorize %s?"
msgstr "Autorizovat %s?"

#: src/chatty-window.c:631
msgid "Reject"
msgstr "Odmítnout"

#: src/chatty-window.c:633
msgid "Accept"
msgstr "Přijmout"

#: src/chatty-window.c:638
#, c-format
msgid "Add %s to contact list"
msgstr "Přidat %s do seznamu kontaktů"

#: src/chatty-window.c:668
msgid "Contact added"
msgstr "Kontakt přidán"

#: src/chatty-window.c:671
#, c-format
msgid "User %s has added %s to the contacts"
msgstr "Uživatel %s přidal %s do kontaktů"

#: src/dialogs/chatty-settings-dialog.c:320
msgid "Select Protocol"
msgstr "Vybrat protokol"

#: src/dialogs/chatty-settings-dialog.c:325
msgid "Add XMPP account"
msgstr "Přidat XMPP účet"

#: src/dialogs/chatty-settings-dialog.c:356
msgid "connected"
msgstr "připojeno"

#: src/dialogs/chatty-settings-dialog.c:358
msgid "connecting…"
msgstr "připojování…"

#: src/dialogs/chatty-settings-dialog.c:360
msgid "disconnected"
msgstr "odpojeno"

#: src/dialogs/chatty-settings-dialog.c:444
#: src/dialogs/chatty-user-info-dialog.c:72
msgid "Set Avatar"
msgstr "Nastavit avatara"

#: src/dialogs/chatty-settings-dialog.c:514
#: src/ui/chatty-settings-dialog.ui:468
msgid "Delete Account"
msgstr "Odstranit účet"

#: src/dialogs/chatty-settings-dialog.c:517
#, c-format
msgid "Delete account %s?"
msgstr "Odstranit účet %s?"

#: src/dialogs/chatty-user-info-dialog.c:206
msgid "Encryption not available"
msgstr "Šifrování není dostupné"

#: src/dialogs/chatty-user-info-dialog.c:250
#: src/dialogs/chatty-user-info-dialog.c:256
msgid "This chat is not encrypted"
msgstr "Tato konverzace není šifrována"

#: src/dialogs/chatty-user-info-dialog.c:253
msgid "Encryption is not available"
msgstr "Šifrování není dostupné"

#: src/dialogs/chatty-user-info-dialog.c:260
msgid "This chat is encrypted"
msgstr "Tato konverzace je šifrována"

#: src/dialogs/chatty-user-info-dialog.c:470
msgid "Phone Number:"
msgstr "Telefonní číslo:"

#: src/dialogs/chatty-muc-info-dialog.c:335
msgid "members"
msgstr "členové"

#: src/ui/chatty-dialog-join-muc.ui:12 src/ui/chatty-window.ui:19
msgid "New Group Chat"
msgstr "Nová hromadná konverzace"

#: src/ui/chatty-dialog-join-muc.ui:28
msgid "Join Chat"
msgstr "Připojit ke konverzaci"

#: src/ui/chatty-dialog-join-muc.ui:81 src/ui/chatty-dialog-new-chat.ui:236
msgid "Select chat account"
msgstr "Zvolit účet"

#: src/ui/chatty-dialog-join-muc.ui:155
msgid "Password (optional)"
msgstr "Heslo (nepovinné)"

#: src/ui/chatty-dialog-muc-info.ui:26
msgid "Group Details"
msgstr "Podrobnosti skupiny"

#: src/ui/chatty-dialog-muc-info.ui:52
msgid "Invite Contact"
msgstr "Pozvat kontakt"

#: src/ui/chatty-dialog-muc-info.ui:69
msgid "Invite"
msgstr "Pozvat"

#: src/ui/chatty-dialog-muc-info.ui:166
msgid "Room topic"
msgstr "Téma místnosti"

#: src/ui/chatty-dialog-muc-info.ui:225
msgid "Room settings"
msgstr "Nastavení místnosti"

#: src/ui/chatty-dialog-muc-info.ui:248 src/ui/chatty-dialog-user-info.ui:210
msgid "Notifications"
msgstr "Upozornění"

#: src/ui/chatty-dialog-muc-info.ui:249
msgid "Show notification badge"
msgstr "Ukázat upozorňující znamení"

#: src/ui/chatty-dialog-muc-info.ui:265
msgid "Status Messages"
msgstr "Stavové zprávy"

#: src/ui/chatty-dialog-muc-info.ui:266
msgid "Show status messages in chat"
msgstr "Zobrazit stavové zprávy v konverzaci"

#: src/ui/chatty-dialog-muc-info.ui:289
msgid "0 members"
msgstr "0 členů"

#: src/ui/chatty-dialog-muc-info.ui:385
msgid "Invite Message"
msgstr "Pozvánka"

#: src/ui/chatty-dialog-new-chat.ui:26
msgid "Start Chat"
msgstr "Začít konverzaci"

#: src/ui/chatty-dialog-new-chat.ui:55
msgid "New Contact"
msgstr "Nový kontakt"

#: src/ui/chatty-dialog-new-chat.ui:83 src/ui/chatty-window.ui:112
msgid "Add Contact"
msgstr "Přidat kontakt"

#: src/ui/chatty-dialog-new-chat.ui:148
msgid "Send To:"
msgstr ""

#: src/ui/chatty-dialog-new-chat.ui:168
msgid "Send to"
msgstr "Příjemce"

#: src/ui/chatty-dialog-new-chat.ui:287
msgid "Name (optional)"
msgstr "Jméno (volitelně)"

#: src/ui/chatty-dialog-new-chat.ui:325 src/ui/chatty-window.ui:138
msgid "Add to Contacts"
msgstr ""

#: src/ui/chatty-dialog-user-info.ui:12 src/ui/chatty-window.ui:98
msgid "Chat Details"
msgstr "Podrobnosti konverzace"

#: src/ui/chatty-dialog-user-info.ui:97
msgid "XMPP ID"
msgstr "XMPP ID"

#: src/ui/chatty-dialog-user-info.ui:112 src/ui/chatty-dialog-user-info.ui:226
msgid "Encryption"
msgstr "Šifrování"

#: src/ui/chatty-dialog-user-info.ui:127 src/ui/chatty-settings-dialog.ui:381
msgid "Status"
msgstr "Status"

#: src/ui/chatty-dialog-user-info.ui:227
msgid "Secure messaging using OMEMO"
msgstr "Bezpečná komunikace pomocí OMEMO"

#: src/ui/chatty-dialog-user-info.ui:249
msgid "Fingerprints"
msgstr "Otisky"

#: src/ui/chatty-message-list-popover.ui:18
msgid "Copy"
msgstr "Kopírovat"

#: src/ui/chatty-pane-msg-view.ui:14
msgid "Unencrypted"
msgstr "Nešifrováno"

#: src/ui/chatty-settings-dialog.ui:12 src/ui/chatty-window.ui:57
msgid "Preferences"
msgstr "Předvolby"

#: src/ui/chatty-settings-dialog.ui:21
msgid "Back"
msgstr "Zpět"

#: src/ui/chatty-settings-dialog.ui:41
msgid "_Add"
msgstr "_Přidat"

#: src/ui/chatty-settings-dialog.ui:57
msgid "_Save"
msgstr "_Uložit"

#: src/ui/chatty-settings-dialog.ui:91
msgid "Accounts"
msgstr "Účty"

#: src/ui/chatty-settings-dialog.ui:105
msgid "Add new account…"
msgstr "Přidat nový účet…"

#: src/ui/chatty-settings-dialog.ui:117
msgid "Privacy"
msgstr "Soukromí"

#: src/ui/chatty-settings-dialog.ui:122
msgid "Message Receipts"
msgstr "Doručenky"

#: src/ui/chatty-settings-dialog.ui:123
msgid "Confirm received messages"
msgstr "Potvrdit doručení zprávy"

#: src/ui/chatty-settings-dialog.ui:136
msgid "Message Archive Management"
msgstr "Správa archivu zpráv"

#: src/ui/chatty-settings-dialog.ui:137
msgid "Sync conversations with chat server"
msgstr "Synchronizovat konverzace s chat serverem"

#: src/ui/chatty-settings-dialog.ui:150
msgid "Message Carbon Copies"
msgstr "Kopie zpráv"

#: src/ui/chatty-settings-dialog.ui:164
msgid "Typing Notification"
msgstr "Upozornění na psaní"

#: src/ui/chatty-settings-dialog.ui:165
msgid "Send typing messages"
msgstr "Odesílat zprávy o odepisování"

#: src/ui/chatty-settings-dialog.ui:181
msgid "Chats List"
msgstr "Seznam konverzací"

#: src/ui/chatty-settings-dialog.ui:186
msgid "Indicate Offline Contacts"
msgstr "Označit off-line kontakty"

#: src/ui/chatty-settings-dialog.ui:187
msgid "Grey out avatars from offline contacts"
msgstr "Zašedivět profilovky off-line kontaktů"

#: src/ui/chatty-settings-dialog.ui:200
msgid "Indicate Idle Contacts"
msgstr "Označit nečinné kontakty"

#: src/ui/chatty-settings-dialog.ui:201
msgid "Blur avatars from offline contacts"
msgstr "Rozmazat profilovky off-line kontaktů"

#: src/ui/chatty-settings-dialog.ui:214
msgid "Indicate Unknown Contacts"
msgstr "Označit neznámé kontakty"

#: src/ui/chatty-settings-dialog.ui:215
msgid "Color unknown contact ID red"
msgstr "Zabarvit ID neznámého kontaktu do červena"

#: src/ui/chatty-settings-dialog.ui:231
msgid "Editor"
msgstr "Editor"

#: src/ui/chatty-settings-dialog.ui:236
msgid "Graphical Emoticons"
msgstr "Grafické emotikony"

#: src/ui/chatty-settings-dialog.ui:237
msgid "Convert ASCII emoticons"
msgstr "Konvertovat ASCII emotikony"

#: src/ui/chatty-settings-dialog.ui:250
msgid "Return = Send Message"
msgstr "Enter = Odeslat zprávu"

#: src/ui/chatty-settings-dialog.ui:251
msgid "Send message with return key"
msgstr "Odesílat zprávy klávesou Enter"

#: src/ui/chatty-settings-dialog.ui:319
msgid "Account ID"
msgstr "ID účtu"

#: src/ui/chatty-settings-dialog.ui:352
msgid "Protocol"
msgstr "Protokol"

#: src/ui/chatty-settings-dialog.ui:410 src/ui/chatty-settings-dialog.ui:707
msgid "Password"
msgstr "Heslo"

#: src/ui/chatty-settings-dialog.ui:485
msgid "Own Fingerprint"
msgstr "Vlastní otisk"

#: src/ui/chatty-settings-dialog.ui:511
msgid "Other Devices"
msgstr "Jiná zařízení"

#: src/ui/chatty-settings-dialog.ui:595
msgid "XMPP"
msgstr "XMPP"

#: src/ui/chatty-settings-dialog.ui:609
msgid "Matrix"
msgstr "Matrix"

#: src/ui/chatty-settings-dialog.ui:624
msgid "Telegram"
msgstr "Telegram"

#: src/ui/chatty-settings-dialog.ui:682
msgid "Provider"
msgstr "Poskytovatel"

#: src/ui/chatty-window.ui:32
msgid "New Direct Chat"
msgstr "Nová přímá konverzace"

#: src/ui/chatty-window.ui:70
msgid "About Chats"
msgstr "O aplikaci Chats"

#: src/ui/chatty-window.ui:163
msgid "Leave Chat"
msgstr "Odejít z konverzace"

#: src/ui/chatty-window.ui:176
msgid "Delete Chat"
msgstr "Smazat konverzaci"

#: src/users/chatty-contact.c:405
msgid "Mobile"
msgstr "Mobil"

#: src/users/chatty-contact.c:407
msgid "Work"
msgstr "Zaměstnání"

#: src/users/chatty-contact.c:409
msgid "Home"
msgstr "Domov"

#: src/users/chatty-contact.c:411
msgid "Other"
msgstr "Ostatní"
