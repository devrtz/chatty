# Brazilian Portuguese translaiton to chatty.
# Copyright (C) 2020 THE chatty'S COPYRIGHT HOLDER
# This file is distributed under the same license as the chatty package.
# Luís Fernando Stürmer da Rosa <luisfsr@dismail.de>, 2018. #zanata
# Luís Fernando Stürmer da Rosa <luisfsr@dismail.de>, 2019. #zanata
# Luís Fernando Stürmer da Rosa <luisfsr@dismail.de>, 2020. #zanata
# Rafael Fontenelle <rafaelff@gnome.org>, 2020.
# Luís Fernando Stürmer da Rosa <luisfsr@dismail.de>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: purism-chatty\n"
"Report-Msgid-Bugs-To: https://source.puri.sm/Librem5/chatty/issues\n"
"POT-Creation-Date: 2020-07-30 03:24+0000\n"
"PO-Revision-Date: 2020-07-30 11:11-0300\n"
"Last-Translator: Luís Fernando Stürmer da Rosa <luisfsr@dismail.de>\n"
"Language-Team: Brazilian Portuguese <gnome-pt_br-list@gnome.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.3.1\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: data/sm.puri.Chatty.desktop.in:3 data/sm.puri.Chatty.metainfo.xml.in:6
#: src/chatty-application.c:273 src/ui/chatty-window.ui:224
msgid "Chats"
msgstr "Conversas"

#: data/sm.puri.Chatty.desktop.in:5
msgid "sm.puri.Chatty"
msgstr "sm.puri.Chatty"

#: data/sm.puri.Chatty.desktop.in:6
msgid "SMS and XMPP chat application"
msgstr "Aplicativo de conversa via XMPP e SMS"

#: data/sm.puri.Chatty.desktop.in:7
msgid "XMPP;SMS;chat;jabber;messaging;modem"
msgstr "XMPP;SMS;chat;jabber;messaging;modem"

#: data/sm.puri.Chatty.gschema.xml:7 data/sm.puri.Chatty.gschema.xml:8
msgid "Whether the application is launching the first time"
msgstr "Se o aplicativo está rodando pela primeira vez"

#: data/sm.puri.Chatty.gschema.xml:13
msgid "Country code in ISO 3166-1 alpha-2 format"
msgstr "Código internacional no formato ISO 3166-1 alpha-2"

#: data/sm.puri.Chatty.gschema.xml:14
msgid "Two letter country code of the last available SIM/Network"
msgstr "Código internacional de duas letras do último cartão SIM utilizado"

#: data/sm.puri.Chatty.gschema.xml:19
msgid "Send message read receipts"
msgstr "Enviar confirmações de recebimento"

#: data/sm.puri.Chatty.gschema.xml:20
msgid "Whether to send the status of the message if read"
msgstr "Se é para enviar o status da mensagem lida"

#: data/sm.puri.Chatty.gschema.xml:25
msgid "Message carbon copies"
msgstr "Cópias de mensagens"

#: data/sm.puri.Chatty.gschema.xml:26 src/ui/chatty-settings-dialog.ui:153
msgid "Share chat history among devices"
msgstr "Sincronizar histórico entre dispositivos"

#: data/sm.puri.Chatty.gschema.xml:31
msgid "Enable Message Archive Management"
msgstr "Habilitar o gerenciador de mensagens arquivadas"

#: data/sm.puri.Chatty.gschema.xml:32
msgid "Enable MAM archive synchronization from the server"
msgstr "Habilitar a sincronia de mensagens arquivadas pelo servidor"

#: data/sm.puri.Chatty.gschema.xml:37
msgid "Send typing notifications"
msgstr "Enviar notificação de escrita"

#: data/sm.puri.Chatty.gschema.xml:38
msgid "Whether to Send typing notifications"
msgstr "Se é para enviar notificações de digitação"

#: data/sm.puri.Chatty.gschema.xml:43 data/sm.puri.Chatty.gschema.xml:44
msgid "Mark offline users differently"
msgstr "Diferenciar usuários desconectados"

#: data/sm.puri.Chatty.gschema.xml:49 data/sm.puri.Chatty.gschema.xml:50
msgid "Mark Idle users differently"
msgstr "Diferenciar usuários ociosos"

#: data/sm.puri.Chatty.gschema.xml:55 data/sm.puri.Chatty.gschema.xml:56
msgid "Indicate unknown contacts"
msgstr "Indicar contatos não conhecidos"

#: data/sm.puri.Chatty.gschema.xml:61
msgid "Convert text to emoticons"
msgstr "Converter textos em emoticons"

#: data/sm.puri.Chatty.gschema.xml:62
msgid "Convert text matching emoticons as real emoticons"
msgstr "Converter emoticons de texto para emoticons verdadeiros"

#: data/sm.puri.Chatty.gschema.xml:67
msgid "Enter key sends the message"
msgstr "ENTER envia a mensagem"

#: data/sm.puri.Chatty.gschema.xml:68
msgid "Whether pressing Enter key sends the message"
msgstr "Se é para enviar ao pressionar ENTER"

#: data/sm.puri.Chatty.gschema.xml:73
msgid "Window maximized"
msgstr "Janela maximizada"

#: data/sm.puri.Chatty.gschema.xml:74
msgid "Window maximized state"
msgstr "Estado de janela maximizada"

#: data/sm.puri.Chatty.gschema.xml:79
msgid "Window size"
msgstr "Tamanho da janela"

#: data/sm.puri.Chatty.gschema.xml:80
msgid "Window size (width, height)."
msgstr "Tamanho da janela (largura, altura)."

#: data/sm.puri.Chatty.metainfo.xml.in:7
msgid "A messaging application"
msgstr "Um aplicativo de mensagens"

#: data/sm.puri.Chatty.metainfo.xml.in:9
msgid "Chats is a messaging application supporting XMPP and SMS."
msgstr "Conversas é um aplicativo compatível com XMPP e SMS."

#: data/sm.puri.Chatty.metainfo.xml.in:16
msgid "Chats message window"
msgstr "Janela de mensagens"

#: src/chatty-application.c:68
msgid "Show release version"
msgstr "Exibir versão"

#: src/chatty-application.c:69
msgid "Start in daemon mode"
msgstr "Inicializar em segundo plano"

#: src/chatty-application.c:70
msgid "Disable all accounts"
msgstr "Desabilitar contas"

#: src/chatty-application.c:71
msgid "Enable libpurple debug messages"
msgstr "Habilitar messagens de depuração do libpurple"

#: src/chatty-application.c:72
msgid "Enable verbose libpurple debug messages"
msgstr "Habilitar mensagens de depuração detalhada do libpurple"

#: src/chatty-application.c:108
#, c-format
msgid "Authorize %s?"
msgstr "Autorizar %s?"

#: src/chatty-application.c:112
msgid "Reject"
msgstr "Rejeitar"

#: src/chatty-application.c:114
msgid "Accept"
msgstr "Aceitar"

#: src/chatty-application.c:119
#, c-format
msgid "Add %s to contact list"
msgstr "Adicionar %s à lista de contatos"

#: src/chatty-application.c:142
msgid "Contact added"
msgstr "Contato adicionado"

#: src/chatty-application.c:145
#, c-format
msgid "User %s has added %s to the contacts"
msgstr "O usuário %s adicionou %s aos contatos"

#: src/chatty-application.c:165
msgid "Login failed"
msgstr "O login falhou"

#: src/chatty-application.c:171
msgid "Please check ID and password"
msgstr "Verifique o ID e a senha"

#: src/chatty-chat-view.c:70 src/chatty-chat-view.c:75
msgid "This is an IM conversation."
msgstr "Esta é uma conversa IM."

#: src/chatty-chat-view.c:71 src/chatty-chat-view.c:81
msgid "Your messages are not encrypted,"
msgstr "Suas mensagens não estão criptografadas,"

#: src/chatty-chat-view.c:72
msgid "ask your counterpart to use E2EE."
msgstr "convide seu interlocutor a usar o E2EE."

#: src/chatty-chat-view.c:76
msgid "Your messages are secured"
msgstr "Suas mensagens estão protegidas"

#: src/chatty-chat-view.c:77
msgid "by end-to-end encryption."
msgstr "por criptografia ponta a ponta."

#: src/chatty-chat-view.c:80
msgid "This is an SMS conversation."
msgstr "Esta é uma conversa SMS."

#: src/chatty-chat-view.c:82
msgid "and carrier rates may apply."
msgstr "e taxas podem ser cobradas."

#: src/chatty-list-row.c:72
msgid "s"
msgstr "s"

#: src/chatty-list-row.c:73 src/chatty-list-row.c:74
msgid "m"
msgstr "m"

#: src/chatty-list-row.c:75 src/chatty-list-row.c:76
msgid "h"
msgstr "h"

#: src/chatty-list-row.c:77 src/chatty-list-row.c:78
msgid "d"
msgstr "d"

#: src/chatty-list-row.c:79
msgid "mo"
msgstr "m"

#: src/chatty-list-row.c:80
msgid "mos"
msgstr "ms"

#: src/chatty-list-row.c:81 src/chatty-list-row.c:82
msgid "y"
msgstr "a"

#: src/chatty-list-row.c:183
msgid "Over"
msgstr "Sobre"

#: src/chatty-list-row.c:187
msgid "Almost"
msgstr "Quase"

#: src/chatty-list-row.c:204
msgid "Owner"
msgstr "Dono"

#: src/chatty-list-row.c:207
msgid "Moderator"
msgstr "Moderador"

#: src/chatty-list-row.c:210
msgid "Member"
msgstr "Participante"

#: src/chatty-manager.c:991
#, c-format
msgid "New message from %s"
msgstr "Nova mensagem de %s"

#: src/chatty-message-row.c:89
msgid "Copy"
msgstr "Copiar"

#: src/chatty-notify.c:81
msgid "Open Message"
msgstr "Abrir mensagem"

#: src/chatty-notify.c:86
msgid "Message Received"
msgstr "Mensagem recebida"

#: src/chatty-notify.c:92
msgid "Message Error"
msgstr "Erro na mensagem"

#: src/chatty-notify.c:98
msgid "Account Info"
msgstr "Informações da conta"

#: src/chatty-notify.c:104
msgid "Account Connected"
msgstr "Conta conectada"

#: src/chatty-notify.c:111
msgid "Open Account Settings"
msgstr "Abrir configurações da conta"

#: src/chatty-notify.c:114
msgid "Account Disconnected"
msgstr "Conta desconectada"

#: src/chatty-purple-notify.c:42
msgid "Close"
msgstr "Fechar"

#: src/chatty-purple-request.c:186
msgid "Save File..."
msgstr "Salvar arquivo..."

#: src/chatty-purple-request.c:187
msgid "Open File..."
msgstr "Abrir arquivo..."

#: src/chatty-purple-request.c:191 src/chatty-window.c:656
#: src/dialogs/chatty-settings-dialog.c:447
#: src/dialogs/chatty-user-info-dialog.c:66 src/ui/chatty-dialog-join-muc.ui:16
#: src/ui/chatty-dialog-muc-info.ui:56
msgid "Cancel"
msgstr "Cancelar"

#: src/chatty-purple-request.c:193
msgid "Save"
msgstr "Salvar"

#: src/chatty-purple-request.c:193 src/dialogs/chatty-settings-dialog.c:446
#: src/dialogs/chatty-user-info-dialog.c:65
msgid "Open"
msgstr "Abrir"

#. TRANSLATORS: Time format with time in AM/PM format
#: src/chatty-utils.c:291
msgid "%I:%M %p"
msgstr "%I:%M %p"

#. TRANSLATORS: Time format as supported by g_date_time_format()
#: src/chatty-utils.c:296
msgid "%A %R"
msgstr "%A %R"

#. TRANSLATORS: Time format with day and time in AM/PM format
#: src/chatty-utils.c:299
msgid "%A %I:%M %p"
msgstr "%A %I:%M %p"

#. TRANSLATORS: Year format as supported by g_date_time_format()
#: src/chatty-utils.c:303
msgid "%Y-%m-%d"
msgstr "%Y-%m-%d"

#: src/chatty-window.c:100 src/chatty-window.c:105 src/chatty-window.c:110
msgid "Choose a contact"
msgstr "Escolha um contato"

#: src/chatty-window.c:101
msgid ""
"Select an <b>SMS</b> or <b>Instant Message</b> contact with the <b>\"+\"</b> "
"button in the titlebar."
msgstr ""
"Escolha por um contato via <b>SMS</b> ou <b>mensagem instantânea</b> com o "
"botão <b>\"+\"</b> na barra de título."

#: src/chatty-window.c:106
msgid ""
"Select an <b>Instant Message</b> contact with the \"+\" button in the "
"titlebar."
msgstr ""
"Escolha um contato de <b>mensagem instantânea</b> com o botão \"+\" na barra "
"de título."

#: src/chatty-window.c:111
msgid "Start a <b>SMS</b> chat with the \"+\" button in the titlebar."
msgstr ""
"Comece uma conversa via <b>SMS</b> tocando no botão \"+\" na barra de título."

#: src/chatty-window.c:112 src/chatty-window.c:116
msgid ""
"For <b>Instant Messaging</b> add or activate an account in <i>\"preferences"
"\"</i>."
msgstr ""
"Para mandar <b>mensagens instantâneas</b> crie ou ative uma conta em <i>"
"\"preferências\"</i>."

#: src/chatty-window.c:115
msgid "Start chatting"
msgstr "Comece a conversar"

#: src/chatty-window.c:639
msgid "Disconnect group chat"
msgstr "Sair da conversa em grupo"

#: src/chatty-window.c:640
msgid "This removes chat from chats list"
msgstr "Isto removerá a conversa da lista"

#: src/chatty-window.c:644
msgid "Delete chat with"
msgstr "Apagar conversa com"

#: src/chatty-window.c:645
msgid "This deletes the conversation history"
msgstr "Isto fará com que o histórico de conversas seja apagado"

#: src/chatty-window.c:658
msgid "Delete"
msgstr "Apagar"

#: src/chatty-window.c:910
msgid "An SMS and XMPP messaging client"
msgstr "Um aplicativo de conversa via XMPP e SMS"

#: src/chatty-window.c:917
msgid "translator-credits"
msgstr ""
"Luís Fernando Stürmer da Rosa <luisfsr@dismail.de>\n"
"Rafael Fontenelle <rafaelff@gnome.org>"

#: src/dialogs/chatty-muc-info-dialog.c:324
msgid "members"
msgstr "participantes"

#: src/dialogs/chatty-new-chat-dialog.c:134
msgid "Send To"
msgstr "Enviar para"

#: src/dialogs/chatty-new-chat-dialog.c:178
#, c-format
msgid "Error opening GNOME Contacts: %s"
msgstr "Erro ao abrir o aplicativo Contatos: %s"

#: src/dialogs/chatty-settings-dialog.c:319
msgid "Select Protocol"
msgstr "Selecione o protocolo"

#: src/dialogs/chatty-settings-dialog.c:324
msgid "Add XMPP account"
msgstr "Adicionar conta XMPP"

#: src/dialogs/chatty-settings-dialog.c:355
msgid "connected"
msgstr "contectado"

#: src/dialogs/chatty-settings-dialog.c:357
msgid "connecting…"
msgstr "conectando…"

#: src/dialogs/chatty-settings-dialog.c:359
msgid "disconnected"
msgstr "desconectado"

#: src/dialogs/chatty-settings-dialog.c:443
#: src/dialogs/chatty-user-info-dialog.c:62
msgid "Set Avatar"
msgstr "Configurar avatar"

#: src/dialogs/chatty-settings-dialog.c:513
#: src/ui/chatty-settings-dialog.ui:477
msgid "Delete Account"
msgstr "Apagar conta"

#: src/dialogs/chatty-settings-dialog.c:516
#, c-format
msgid "Delete account %s?"
msgstr "Apagar conta %s?"

#: src/dialogs/chatty-user-info-dialog.c:144
msgid "Encryption not available"
msgstr "Criptografia não disponível"

#: src/dialogs/chatty-user-info-dialog.c:182
msgid "Encryption is not available"
msgstr "Criptografia não disponível"

#: src/dialogs/chatty-user-info-dialog.c:184
msgid "This chat is encrypted"
msgstr "Esta conversa está criptografada"

#: src/dialogs/chatty-user-info-dialog.c:186
msgid "This chat is not encrypted"
msgstr "Esta conversa não está criptografada"

#: src/dialogs/chatty-user-info-dialog.c:256
msgid "Phone Number:"
msgstr "Número do telefone:"

#: src/ui/chatty-dialog-join-muc.ui:12
msgid "New Group Chat"
msgstr "Novo grupo de conversa"

#: src/ui/chatty-dialog-join-muc.ui:28
msgid "Join Chat"
msgstr "Participar da conversa"

#: src/ui/chatty-dialog-join-muc.ui:81 src/ui/chatty-dialog-new-chat.ui:237
msgid "Select chat account"
msgstr "Selecionar uma conta"

#: src/ui/chatty-dialog-join-muc.ui:155
msgid "Password (optional)"
msgstr "Senha (opcional)"

#: src/ui/chatty-dialog-muc-info.ui:26
msgid "Group Details"
msgstr "Detalhes do grupo"

#: src/ui/chatty-dialog-muc-info.ui:52
msgid "Invite Contact"
msgstr "Convidar contato"

#: src/ui/chatty-dialog-muc-info.ui:69
msgid "Invite"
msgstr "Convidar"

#: src/ui/chatty-dialog-muc-info.ui:166
msgid "Room topic"
msgstr "Assunto da sala"

#: src/ui/chatty-dialog-muc-info.ui:225
msgid "Room settings"
msgstr "Configurações da sala"

#: src/ui/chatty-dialog-muc-info.ui:248 src/ui/chatty-dialog-user-info.ui:209
msgid "Notifications"
msgstr "Notificações"

#: src/ui/chatty-dialog-muc-info.ui:249
msgid "Show notification badge"
msgstr "Exibir barra de notificações"

#: src/ui/chatty-dialog-muc-info.ui:265
msgid "Status Messages"
msgstr "Mensagens de status"

#: src/ui/chatty-dialog-muc-info.ui:266
msgid "Show status messages in chat"
msgstr "Exibir mensagens de status na conversa"

#: src/ui/chatty-dialog-muc-info.ui:289
msgid "0 members"
msgstr "0 participante"

#: src/ui/chatty-dialog-muc-info.ui:377
msgid "Invite Message"
msgstr "Mensagem de convite"

#: src/ui/chatty-dialog-new-chat.ui:26
msgid "Start Chat"
msgstr "Começar uma conversa"

#: src/ui/chatty-dialog-new-chat.ui:55
msgid "New Contact"
msgstr "Novo contato"

#: src/ui/chatty-dialog-new-chat.ui:83 src/ui/chatty-window.ui:125
msgid "Add Contact"
msgstr "Criar contato"

#: src/ui/chatty-dialog-new-chat.ui:149
msgid "Send To:"
msgstr "Enviar para:"

#: src/ui/chatty-dialog-new-chat.ui:288
msgid "Name (optional)"
msgstr "Nome (opcional)"

#: src/ui/chatty-dialog-new-chat.ui:328 src/ui/chatty-window.ui:151
msgid "Add to Contacts"
msgstr "Adicionar aos contatos"

#: src/ui/chatty-dialog-user-info.ui:12 src/ui/chatty-window.ui:111
msgid "Chat Details"
msgstr "Detalhes da conversa"

#: src/ui/chatty-dialog-user-info.ui:96
msgid "XMPP ID"
msgstr "ID XMPP"

#: src/ui/chatty-dialog-user-info.ui:111 src/ui/chatty-dialog-user-info.ui:225
msgid "Encryption"
msgstr "Criptografia"

#: src/ui/chatty-dialog-user-info.ui:126 src/ui/chatty-settings-dialog.ui:390
msgid "Status"
msgstr "Estado"

#: src/ui/chatty-dialog-user-info.ui:226
msgid "Secure messaging using OMEMO"
msgstr "Proteja suas conversas usando OMEMO"

#: src/ui/chatty-dialog-user-info.ui:247
msgid "Fingerprints"
msgstr "Assinaturas digitais"

#: src/ui/chatty-settings-dialog.ui:12 src/ui/chatty-window.ui:18
msgid "Preferences"
msgstr "Preferências"

#: src/ui/chatty-settings-dialog.ui:21
msgid "Back"
msgstr "Voltar"

#: src/ui/chatty-settings-dialog.ui:41
msgid "_Add"
msgstr "_Adicionar"

#: src/ui/chatty-settings-dialog.ui:57
msgid "_Save"
msgstr "_Salvar"

#: src/ui/chatty-settings-dialog.ui:91
msgid "Accounts"
msgstr "Contas"

#: src/ui/chatty-settings-dialog.ui:105
msgid "Add new account…"
msgstr "Adicionar nova conta…"

#: src/ui/chatty-settings-dialog.ui:117
msgid "Privacy"
msgstr "Privacidade"

#: src/ui/chatty-settings-dialog.ui:122
msgid "Message Receipts"
msgstr "Confirmações de recebimento"

#: src/ui/chatty-settings-dialog.ui:123
msgid "Confirm received messages"
msgstr "Confirmar mensagens recebidas"

#: src/ui/chatty-settings-dialog.ui:137
msgid "Message Archive Management"
msgstr "Gerenciador de mensagens arquivadas"

#: src/ui/chatty-settings-dialog.ui:138
msgid "Sync conversations with chat server"
msgstr "Sincronizar conversas com o servidor"

#: src/ui/chatty-settings-dialog.ui:152
msgid "Message Carbon Copies"
msgstr "Copiar as mensagens"

#: src/ui/chatty-settings-dialog.ui:167
msgid "Typing Notification"
msgstr "Notificar digitação"

#: src/ui/chatty-settings-dialog.ui:168
msgid "Send typing messages"
msgstr "Enviar mensagens ao digitá-las"

#: src/ui/chatty-settings-dialog.ui:185
msgid "Chats List"
msgstr "Lista de conversas"

#: src/ui/chatty-settings-dialog.ui:190
msgid "Indicate Offline Contacts"
msgstr "Indicar contatos desconectados"

#: src/ui/chatty-settings-dialog.ui:191
msgid "Grey out avatars from offline contacts"
msgstr "Acinzentar avatares de contatos desconectados"

#: src/ui/chatty-settings-dialog.ui:205
msgid "Indicate Idle Contacts"
msgstr "Indicar contatos inativos"

#: src/ui/chatty-settings-dialog.ui:206
msgid "Blur avatars from idle contacts"
msgstr "Desfocar avatares de contatos desconectados"

#: src/ui/chatty-settings-dialog.ui:220
msgid "Indicate Unknown Contacts"
msgstr "Indicar contatos desconhecidos"

#: src/ui/chatty-settings-dialog.ui:221
msgid "Color unknown contact ID red"
msgstr "Avermelhar contatos desconhecidos"

#: src/ui/chatty-settings-dialog.ui:238
msgid "Editor"
msgstr "Editor"

#: src/ui/chatty-settings-dialog.ui:243
msgid "Graphical Emoticons"
msgstr "Emoticons gráficos"

#: src/ui/chatty-settings-dialog.ui:244
msgid "Convert ASCII emoticons"
msgstr "Converter emoticons ASCII"

#: src/ui/chatty-settings-dialog.ui:258
msgid "Return = Send Message"
msgstr "Enter envia a mensagem"

#: src/ui/chatty-settings-dialog.ui:259
msgid "Send message with return key"
msgstr "Enviar a mensagem com o botão enter"

#: src/ui/chatty-settings-dialog.ui:328
msgid "Account ID"
msgstr "ID da conta"

#: src/ui/chatty-settings-dialog.ui:361
msgid "Protocol"
msgstr "Protocolo"

#: src/ui/chatty-settings-dialog.ui:419 src/ui/chatty-settings-dialog.ui:716
msgid "Password"
msgstr "Senha"

#: src/ui/chatty-settings-dialog.ui:494
msgid "Own Fingerprint"
msgstr "Minha digital"

#: src/ui/chatty-settings-dialog.ui:520
msgid "Other Devices"
msgstr "Outros dispositivos"

#: src/ui/chatty-settings-dialog.ui:604
msgid "XMPP"
msgstr "XMPP"

#: src/ui/chatty-settings-dialog.ui:618
msgid "Matrix"
msgstr "Matrix"

#: src/ui/chatty-settings-dialog.ui:633
msgid "Telegram"
msgstr "Telegram"

#: src/ui/chatty-settings-dialog.ui:691
msgid "Provider"
msgstr "Provedor"

#: src/ui/chatty-window.ui:31
msgid "About Chats"
msgstr "Sobre o Conversas"

#: src/ui/chatty-window.ui:58
msgid "New Message…"
msgstr "Nova mensagem…"

#: src/ui/chatty-window.ui:71
msgid "New Group Message…"
msgstr "Nova mensagem de grupo…"

#: src/ui/chatty-window.ui:84
msgid "New Bulk SMS…"
msgstr "Novo SMS em massa…"

#: src/ui/chatty-window.ui:176
msgid "Leave Chat"
msgstr "Sair da conversa"

#: src/ui/chatty-window.ui:189
msgid "Delete Chat"
msgstr "Apagar conversa"

#: src/users/chatty-contact.c:313
msgid "Mobile: "
msgstr "No celular: "

#: src/users/chatty-contact.c:315
msgid "Work: "
msgstr "No trabalho: "

#: src/users/chatty-contact.c:317
msgid "Other: "
msgstr "Outro: "

#~ msgid "Initial development release of chatty with support for xmpp and sms."
#~ msgstr ""
#~ "Versão inicial de desenvolvimento do chatty compatível com xmpp e sms"

#~ msgid "Me: "
#~ msgstr "Eu:"

#~ msgid "About "
#~ msgstr "Sobre"

#~ msgid "Less than "
#~ msgstr "Menos que"

#~ msgid " seconds"
#~ msgstr "segundos"

#~ msgid " minute"
#~ msgstr "minuto"

#~ msgid " minutes"
#~ msgstr "minutos"

#~ msgid " hour"
#~ msgstr "hora"

#~ msgid " hours"
#~ msgstr "horas"

#~ msgid " day"
#~ msgstr "dia"

#~ msgid " days"
#~ msgstr "dias"

#~ msgid " month"
#~ msgstr "mês"

#~ msgid " months"
#~ msgstr "meses"

#~ msgid " year"
#~ msgstr "ano"

#~ msgid " years"
#~ msgstr "anos"

#~ msgid "Unencrypted"
#~ msgstr "Não criptografado"

#~ msgid "New Direct Chat"
#~ msgstr "Nova conversa direta"

#~ msgid "Home"
#~ msgstr "Em casa"
